const express = require('express')
const app = express()
var port = process.env.PORT || 8081;
//app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
const cors = require('cors');//New for Microservice
app.use(cors());//New for Microservice
app.listen(port, () =>
console.log('HTTP Server with Express.js is listening on port:'+port))
app.get('/',(req, res) => {
    res.send("Microservices_2 gateway by Kushagra Sahni and Sonal Jaiswal. Usage: host/datediff?from_date=mm/dd/yyyy&to_date=mm/dd/yyyy");
})
 app.get('/datediff',function(req,res){
     from_date=Date.parse(req.query.from_date)
     to_date=Date.parse(req.query.to_date)
     if(isNaN(from_date) || isNaN(to_date)){
         res.send("Invalid Inputs . Use Date in mm/dd/yyyy or Month dd,yyyy")
         return
      }
 var diffDays=parseInt((to_date - from_date)/(1000*60*60*24));
 res.send(`${diffDays} days or ${Number(diffDays/7).toFixed(1)} weeks`)  
})